from django.views.generic import TemplateView


class TechnicalBreakView(TemplateView):
    template_name = "technical-break.html"
