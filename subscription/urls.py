from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin

from subscription.email.views import EmailSubscriptionCreate
from subscription.email.views import EmailSubscriptionThankyou
from subscription.core.views import TechnicalBreakView


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^email/$', EmailSubscriptionCreate.as_view(), name='email-subscription'),
    url(r'^$', TechnicalBreakView.as_view()),
    url(r'^subscription-thank-you$', EmailSubscriptionThankyou.as_view(), name="email-subscription-thankyou"),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r"^static/(?P<path>.*)$", "django.views.static.serve", {"document_root": settings.STATIC_ROOT})
)
