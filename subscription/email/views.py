from django.views.generic import CreateView
from django.views.generic import TemplateView
from subscription.email.models import EmailSubscription


class EmailSubscriptionCreate(CreateView):
    model = EmailSubscription
    fields = ['name', 'email']
    template_name = "email/form.html"
    success_url = "/subscription-thank-you"


class EmailSubscriptionThankyou(TemplateView):
    template_name = "email/thank-you.html"