from django.db import models
from django.utils.translation import ugettext_lazy as _


class EmailSubscription(models.Model):
    name = models.CharField(max_length=50, verbose_name=_("Name"))
    email = models.EmailField(max_length=150, verbose_name=_("Email"))

    def __str__(self):
        return "{name} <{email}>".format(name=self.name, email=self.email)

    class Meta:
        verbose_name = _("Email Subscription")
        verbose_name_plural = _("Email Subscriptions")
