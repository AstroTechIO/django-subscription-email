from django.contrib import admin

from subscription.email.models import EmailSubscription


class EmailSubscriptionAdmin(admin.ModelAdmin):
    list_display_links = ["email"]
    list_display = ["name", "email"]
    search_fields = ["^name", "email"]


admin.site.register(EmailSubscription, EmailSubscriptionAdmin)